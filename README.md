This is a straightforward implementation of the Wavelet Tree data structure. The
code is mostly taken from https://github.com/nilehmann/wavelet-tree, but has been
extended and rewritten to make it more readable.

The queries are based on [this paper](https://users.dcc.uchile.cl/~jperez/papers/ioiconf16.pdf).
