#include <bits/stdc++.h>
#define int int64_t

using namespace std;
typedef vector<int>::iterator iter;

vector<int> S = {3, 3, 9, 1, 2, 1, 7, 6, 4, 8, 9, 4, 3, 7, 5, 9, 2, 7, 3, 5, 1, 3};

struct WaveletTree {
    //C holds the amount of elements that go to the left side
    //therefore is C[node][i] always mapLeft and C[node][j] always mapRight
    vector<vector<int>> C;
    vector<int> input;
    int size;

    //sigma = size of the alphabet, so max(input) + 1
    WaveletTree(vector<int>& S, int sigma) : C(sigma*2), size(sigma){
        input = S;
        build(S.begin(),S.end(), 0, size-1,1);
    }

    void build(iter begin, iter end, int left, int right, int node){
        //-> leaf
        if(left == right)
            return;

        int mid = left+(right-left)/2;

        C[node].reserve(end-begin+1);
        C[node].push_back(0);
        //this is a prefixsum, which allows us to get the number of elements
        //that go to the left or right in O(1)
        for(iter it = begin; it != end; ++it)
            C[node].push_back(C[node].back() + (*it<=mid));

        iter pivot = stable_partition(begin, end, [=](int i){return i <= mid;});

        //call recursively for left and right side
        build(begin,pivot,left,mid,node*2);
        build(pivot,end,mid+1,right,node*2+1);
    }

    //Count occurences of x until position i
    //occurences between i and j: rank(x,j) - rank(x,i)
    int rank(int x, int i) const{
        //open the interval on the right as it makes the processing easier
        ++i;
        int left = 0, right = size-1, node = 1, mid;
        while(left != right){
            mid = left+(right-left)/2;

            if(x <= mid){
                i = C[node][i]; right = mid; node = 2*node;
            } else {
                i -= C[node][i]; left = mid+1; node = 2*node+1;
            }
        }
        return i;
    }

    //Find the k-th smallest element in [i,j]
    int quantile(int k, int i, int j){
        ++j;
        int left = 0, right = size-1, node = 1, mid;
        while(left != right){
            mid = left+(right-left)/2;

            if(k <= C[node][j]-C[node][i]){
                i = C[node][i]; j = C[node][j]; right = mid; node = 2*node;
            } else {
                k -= C[node][j]-C[node][i]; i -= C[node][i]; j -= C[node][j]; left = mid+1; node = 2*node+1;
            }
        }
        return right;
    }

    int originalLeft, originalRight;
    //Count amount of numbers in range [a,b] between positions [i,j]
    int range(int i, int j, int a, int b){
        //empty range
        if(i > j || a > b){
            return 0;
        }
        originalLeft = i; originalRight = j;
        return range(i,j+1,0,size-1,1);
    }

    int range(int i, int j, int a, int b, int node){
        //ranges do not intersect
        if(originalLeft > b || originalRight < a){
            return 0;
        }
        //completely contained
        if(originalLeft <= a && b <= originalRight){
            return j-i;
        }
        //partially contained, call recursively
        int mid = (a+b)/2, mapLeft = C[node][i], mapRight = C[node][j];
        return range(mapLeft,mapRight,a,mid,node*2) +
                range(i-mapLeft,j-mapRight,mid+1,b,node*2+1);
    }

    void pushBack(int x){
        input.push_back(x);
        int left = 0, right = size-1, node = 1, mid;
        while(left != right){
            mid = left+(right-left)/2;

            C[node].push_back(C[node].back() + (x<=mid));
            if(x <= mid){
                right = mid;
            } else {
                left = mid+1;
            }
            node = 2 * node + (x > mid);
        }
    }
    void popBack(){
        int x = input.back();
        input.pop_back();
        int left = 0, right = size-1, node = 1, mid;
        while(left != right){
            mid = left+(right-left)/2;

            C[node].pop_back();
            if(x <= mid){
                right = mid;
            } else {
                left = mid+1;
            }
            node = 2 * node + (x > mid);
        }
    }
};

signed main(){
    WaveletTree T(S,100);
    cout << T.rank(3,3) << "\n" << T.quantile(1,7,15) << "\n" << T.range(7,16,3,5) << "\n";
    return 0;
}
